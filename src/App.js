import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from "react-router";
import Content from "./components/Content/Content";
import Layout from "./components/Layout/Layout";
import {PAGES} from "./constants";
import EditPages from "./components/EditPages/EditPages";

class App extends Component {

    render() {
        const links = [];
        for (let i = 0; i < (PAGES.length); i++) {
            links.push(
                <Route path={"/" + PAGES[i]} exact component={Content} key={PAGES[i] + '-route'}/>
            )
        }

        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Content}/>
                    {links}
                    <Route path="/admin" exact component={EditPages}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
