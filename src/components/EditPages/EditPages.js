import React, {Component} from 'react';
import {PAGES} from "../../constants";
import axios from '../../axios-content';
import './EditPages.css';

class EditPages extends Component {

    state = {
        page: '',
        title: '',
        content: ''
    };

    changeSelect = event => {
        const page = event.target.value;
        this.setState({page});
    };

    changeTitle = event => {
        const title = event.target.value;
        this.setState({title});
    };

    changeContent = event => {
        const content = event.target.value;
        this.setState({content});
    };

    saveChanges = (event) => {
        event.preventDefault();
        const data = {
            title: this.state.title,
            content: this.state.content
        };
        axios.put('contents/' + this.state.page + '.json', data).then(() => {
            this.props.history.push('/' + this.state.page);
        }).catch(error => {
            console.log(error);
        })
    };

    componentDidUpdate(prevProps, prevState) {

        if (prevState.page !== this.state.page) {
            axios.get('contents/' + this.state.page + '.json').then(response => {
                this.setState({title: response.data.title, content: response.data.content})
            })
        }
    };

    render() {
        const options = PAGES.map(page => {
            return <option key={page} id={page} value={page}>{page}</option>
        });


        return (
            <div>
                <h2>Edit Page</h2>
                <form onSubmit={this.saveChanges} className="EditForm">
                    <select onChange={this.changeSelect} required defaultValue="Choose page">
                        <option disabled hidden>Choose page</option>
                        {options}
                    </select>
                    <label>Title</label>
                    <input type="text" value={this.state.title} onChange={this.changeTitle}/>
                    <label>Content</label>
                    <textarea cols="30" rows="10" value={this.state.content} onChange={this.changeContent}/>
                    <button>Save</button>
                </form>
            </div>
        );
    }
}

export default EditPages;