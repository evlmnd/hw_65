import React from 'react';
import './NavigationItems.css';
import NavigationItem from "./NavigationItem/NavigationItem";
import {PAGES} from '../../../constants';

const links = [];

for (let i = 0; i < (PAGES.length); i++) {
    links.push(
        <NavigationItem key={PAGES[i]} to={"/" + PAGES[i]} exact>{PAGES[i]}</NavigationItem>
    )
}

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/" exact>Home</NavigationItem>
        {links}
        <NavigationItem to="/admin" exact>Edit Pages</NavigationItem>

    </ul>
);


export default NavigationItems;