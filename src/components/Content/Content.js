import React, {Component} from 'react';
import axios from '../../axios-content';

class Content extends Component {

    state = {
        contents: {}
    };

    getContent = () => {
        let path = this.props.match.path;
        if (path === '/') {
            path = '/home'
        }

        axios.get('contents' + path + '.json').then(response => {
            this.setState({contents: response.data})
        })
    };


    componentDidMount() {
        this.getContent();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.path !== prevProps.match.path) {
            this.getContent();
        }
    }

    render() {

        return (
            <div>
                <h3>{this.state.contents.title}</h3>
                <p>{this.state.contents.content}</p>
            </div>
        );
    }
}

export default Content;