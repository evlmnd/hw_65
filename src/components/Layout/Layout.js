import React, {Fragment} from 'react';
import './Layout.css';
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";


const Layout = ({children}) => {
    return (
        <Fragment>
            <NavigationItems/>
            <main className="Layout-Content">
                {/*<Content/>*/}
                {children}
            </main>
        </Fragment>
    );
};

export default Layout;